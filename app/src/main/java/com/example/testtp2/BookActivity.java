package com.example.testtp2;



import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    //Inisialization of variables, objects,...

    Book book;

    EditText type;

    EditText publisher;

    EditText pub;

    EditText name;

    EditText author;

    BookDbHelper bookDbHelper;

    Button sauv;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        bookDbHelper = new BookDbHelper(getApplicationContext());

        //recover the selected book

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        book = (Book) extras.get("bookSelected");

        //
        name = (EditText) findViewById(R.id.nameBook) ;

        author = (EditText) findViewById(R.id.editAuthors) ;

        pub = (EditText) findViewById(R.id.editYear) ;

        type = (EditText) findViewById(R.id.editGenres) ;

        publisher = (EditText) findViewById(R.id.editPublisher) ;

        //The conditons that we have to respect to add a book

        //If the book is not null
        if(book!=null)
        {
            name.setText(book.getTitle());

            author.setText(book.getAuthors());

            pub.setText(book.getYear());

            type.setText(book.getGenres());

            publisher.setText(book.getPublisher());
        }

        sauv = (Button) findViewById(R.id.save);

        //The save button
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                // when it's not null

                if(book!=null) {

                    book.setTitle(name.getText().toString());

                    book.setAuthors(author.getText().toString());

                    book.setYear(pub.getText().toString());

                    book.setGenres(type.getText().toString());

                    book.setPublisher(publisher.getText().toString());
                    //
                    bookDbHelper.updateBook(book);
                }
                //if the title is empty
                else {
                    if(name.getText().toString().isEmpty()){



                        AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                        builder.setMessage("Le titre n'est pas rempli !");
                        builder.setCancelable(true);
                        builder.show();
                        return;

                    }


                    else {
                        book = new Book(name.getText().toString(), author.getText().toString(), pub.getText().toString(), type.getText().toString(), publisher.getText().toString());

                        //Compare if the book exists or not

                        if(!bookDbHelper.addBook(book)){

                            AlertDialog.Builder builder = new AlertDialog.Builder(BookActivity.this);
                            builder.setMessage("Livre Déjà présent");
                            builder.setCancelable(true);
                            builder.show();
                            return;

                        }

                    }

                }

                //I have to replace this by an OnResume function.
                Intent intent = new Intent(BookActivity.this,MainActivity.class);
                startActivity(intent);

            }
        });
    }
}








